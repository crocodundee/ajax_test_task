
test_data = [("10FA0E00", {'field1': 'Low',
                           'field2': '00',
                           'field3': '01',
                           'field4': '00',
                           'field5': '00',
                           'field6': '01',
                           'field7': '00',
                           'field8': 'Very High',
                           'field9': '00',
                           'field10': '00'}),
             ("FFA01200", {'field1': 'High',
                           'field2': '01',
                           'field3': '01',
                           'field4': '70',
                           'field5': '00',
                           'field6': '00',
                           'field7': '00',
                           'field8': 'Medium',
                           'field9': '00',
                           'field10': '00'}),
             ("7B2D2100", {'field1': 'reserved',
                           'field2': '01',
                           'field3': '01',
                           'field4': '30',
                           'field5': '01',
                           'field6': '00',
                           'field7': '01',
                           'field8': 'High',
                           'field9': '01',
                           'field10': '01'}),
            ]


#Format settings - array [sett_byte1 as dict {bit: [size, 'field_name']}, sett_byte2, sett_byte3, sett_byte4]
device_settings = [{0: [3, 'field1'], 
                    3: [1, 'field2'],  
                    4: [1, 'field3'], 
                    5: [3, 'field4']}, 
                   {0: [1, 'field5'],  
                    1: [1, 'field6'], 
                    2: [1, 'field7'],  
                    3: [3, 'field8'], 
                   },
                   {0: [1, 'field9'], 
                    5: [1, 'field10']
                   },
                   {}
                  ]

BITS = [list(byte.keys()) for byte in device_settings]
FIELDS = [list(byte.values()) for byte in device_settings]

field1 = {'0': 'Low',
          '1': 'reserved',
          '2': 'reserved',
          '3': 'reserved',
          '4': 'Medium', 
          '5': 'reserved',
          '6': 'reserved',
          '7': 'High',  
          }
field4 = {'0': '00', 
          '1': '10',
          '2': '20',
          '3': '30',
          '4': '40',
          '5': '50',
          '6': '60',
          '7': '70',
          }
field8 = {'0': 'Very Low',
          '1': 'reserved',
          '2': 'Low',
          '3': 'reserved',
          '4': 'Medium',
          '5': 'High',
          '6': 'reserved',
          '7': 'Very High',
          }


def to_full_byte(byte):
    if byte.__len__() < 8:
        add_bits_len = 8 - byte.__len__()
        byte = '0'*add_bits_len + byte
    return "".join(reversed(byte))


def get_bytes(payload):
    bytes = [payload[i: i + 2] for i in range(0, payload.__len__(), 2)]
    bytes = [format(int(byte, 16), 'b') for byte in bytes]
    bytes = [to_full_byte(byte) for byte in bytes]
    return bytes


def get_field_value(byte):
    byte = "".join(reversed(byte))
    return str(int(byte, 2))


def get_parsed_data_item(field, value):
    if field == 'field1':
        return {'field1': field1[value]}
    elif field == 'field4':
        return {'field4': field4[value]}
    elif field == 'field8':
        return {'field8': field8[value]}
    else:
        return {field: '0' + value}


def get_data_from_payload(payload):
    bytes = get_bytes(payload)
    parsed_data = dict()
    for (bit, field, byte) in zip(BITS, FIELDS, bytes):
        for i in range(bit.__len__()):
            field_val = get_field_value(byte[bit[i]:(bit[i] + field[i][0])])
            parsed_data.update(get_parsed_data_item(field[i][1], field_val))
    return parsed_data
