import pytest
from parsing_bytes import get_data_from_payload, test_data


@pytest.mark.parametrize("payload, expected", test_data)
def test_payload_check(payload, expected):
    assert expected == get_data_from_payload(payload)
